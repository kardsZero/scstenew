import React, { useState } from "react"
import { Link } from "gatsby"
import '../pages/index.css'
// import { useLocation } from "@reach/router";
function Navbar() {
  const [isOpen, setIsOpen] = useState(false)
  //   const location = useLocation();
  //   useEffect(() => {
  //     setIsOpen(false);
  //   }, [location]);
  //   const onActive = ({ isPartiallyCurrent }) => {
  //     return {
  //       textDecoration: isPartiallyCurrent ? "underline" : "none",
  //       fontWeight: isPartiallyCurrent ? "bold" : "normal",
  //     };
  //   };
  return (
    <nav className="flex items-center justify between flex-wrap pr-10 bg-blue-700 sticky top-10 z-10">
      {/* <div className="flex items-center flex-shrink-0 text-white mr-6 lg:mr-72">
        <Link
          to="/"
          className="flex title-font font-medium items-center md:justify-start justify-center text-gray-900"
        >
          <span className="ml-3 text-xl bg-gray-700 rounded-full p-1">k@</span>
        </Link>
      </div> */}
      <div className="block lg:hidden">
        <div className="block lg:hidden">
          <button
            onClick={() => setIsOpen(!isOpen)}
            className="flex items-center px-3 py-2 rounded text-black-500 hover:text-black-400"
            style={{
              paddingLeft: "300px",
            }}
          >
            <div
              className={` fill-current text-lg ${isOpen ? "hidden" : "block"}`}
            >
              ☰
            </div>
            <div
              className={`fill-current h-3 w-3 ${isOpen ? "block" : "hidden"}`}
            >
              ❌
            </div>
          </button>
        </div>
      </div>
      <div
        className={`w-full block flex-grow lg:flex lg:items-center lg:w-auto ${
          isOpen ? "block" : "hidden"
        }`}
      >
        <div className="text-sm lg:flex-grow text-right">
          <Link
            to="/"
            activeClassName="navbar"
            className="text-lg text-white block lg:inline-block no-underline hover:border-b-4 py-3 px-3"
          >
            Home
          </Link>
          <Link
            to="/YTPage"
            activeClassName="navbar"
            className="text-lg text-white block lg:inline-block no-underline hover:border-b-4 py-3 px-3"
          >
            Education
          </Link>
          <Link
            to="/Impact"
            activeClassName="navbar"
            className="text-lg text-white block lg:inline-block no-underline hover:border-b-4 py-3 px-3"
          >
            Impact
          </Link>
          <Link
            to="/Activities"
            activeClassName="navbar"
            className="text-lg text-white block lg:inline-block no-underline hover:border-b-4 py-3 px-3"
          >
            Activities
          </Link>
          <Link
            to="/Stories"
            activeClassName="navbar"
            className="text-lg text-white block lg:inline-block no-underline hover:border-b-4 py-3 px-3"
          >
            Stories
          </Link>
          <Link
            to="/Programmes"
            activeClassName="navbar"
            className="text-lg text-white block lg:inline-block no-underline hover:border-b-4 py-3 px-3"
          >
            Programmes
          </Link>
          <Link
            to="/About"
            activeClassName="navbar"
            className="text-lg text-white block lg:inline-block no-underline hover:border-b-4 py-3 px-3"
          >
            About Us
          </Link>
          <Link
            to="/Contact"
            activeClassName="navbar"
            className="text-lg text-white block lg:inline-block no-underline hover:border-b-4 py-3 px-3"
          >
            Contact Us
          </Link>
          <Link
            to="/Gallery"
            activeClassName="navbar"
            className="text-lg text-white block lg:inline-block no-underline hover:border-b-4 py-3 px-3"
          >
            Gallery
          </Link>
        </div>
      </div>
    </nav>
  )
}
export default Navbar