import React from 'react';
import { StaticImage } from 'gatsby-plugin-image';

const Header = () => {

  return (
      <div className="px-10 py-5 flex justify-between">
            <div className="flex space-x-2">
              <StaticImage className="w-32" src="../images/logo.png" />
              <div className="my-4">
                  <h2 className="text-2xl">State Council of Science Technology & Environment, Meghalaya</h2>
              </div>
            </div>
        <div className="flex space-x-2">
        <h1 className="text-lg text-right my-4">Government of Meghalaya</h1>
        <StaticImage className='w-10 h-16' src="../images/Emblem.png" />
        </div>
      </div>
  );
};

export default Header;
