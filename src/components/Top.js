import React from 'react'

function Top() {

    const handleSkipToMainContent = () => {
        const section = document.querySelector("#main-content")
        const offset = 95 // Change this value to adjust the amount of padding
        const elementPosition = section.getBoundingClientRect().top
        const offsetPosition = elementPosition - offset
      
        window.scrollTo({
          top: offsetPosition,
          behavior: "smooth",
        })
      }

  return (
    <div className='px-10 text-right text-black sticky top-0 z-10 border-b-2 bg-white'>
      <button className='border-x-2 px-2 py-2' onClick={handleSkipToMainContent }>Skip to main content</button>
      <button className='border-r-2 px-2 py-2'>-A</button>
      <button className='border-r-2 px-2 py-2'>A</button>
      <button className='border-r-2 px-2 py-2'>+A</button>
    </div>
  )
}

export default Top
