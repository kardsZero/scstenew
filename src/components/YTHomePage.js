import React, { useState } from "react";
// import "../styles/tailwind.css"; // Make sure to import your Tailwind CSS styles
const videoUrls = [
  "https://www.youtube.com/embed/QeA6CQRjXMM?si=ImWR2d2n9enlNdpY",
  "https://www.youtube.com/embed/hURa27WVw8A?si=rsJC0nlmUBgApdSD",
  "https://www.youtube.com/embed/Vs0qwZdROKw?si=KAXYjhF_WEU4Y7mZ",
  "https://www.youtube.com/embed/N0YzCFyX8J0?si=4i4NL9Vw-RbO7Tjc",
  "https://www.youtube.com/embed/wSk2wdA7y3s?si=diDTVX_mArau6U5x",
  "https://www.youtube.com/embed/U7wcgeSeTXw?si=4EQKdF4HQU8_Nk-7",
  "https://www.youtube.com/embed/R9Fl-c7YfC0?si=eEX5klwxWjVNo6v8",
  // Add more video URLs as needed
];
const IndexPage = () => {
  const [currentVideo, setCurrentVideo] = useState(0);
  const [thumbnailStart, setThumbnailStart] = useState(0);
  const handleVideoChange = (index) => {
    setCurrentVideo(index);
  };
  const handleThumbnailScroll = (direction) => {
    if (direction === "left") {
      setThumbnailStart(
        (prev) => (prev - 1 + videoUrls.length) % videoUrls.length
      );
    } else if (direction === "right") {
      setThumbnailStart((prev) => (prev + 1) % videoUrls.length);
    }
  };
  return (
    <div className="app">
      <h1 className="mb-4 text-center">YouTube Video Carousel</h1>
      <div className="flex flex-col items-center carousel-container">
        <div className="mb-5 video-container">
          <iframe
            title={`Video ${currentVideo + 1}`}
            width="560"
            height="315"
            src={videoUrls[currentVideo]}
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            allowFullScreen
          ></iframe>
        </div>
        <div className="flex thumbnail-container">
          {thumbnailStart > 0 && (
            <button
              className="thumbnail-arrow left-arrow"
              onClick={() => handleThumbnailScroll("left")}
            >
              &lt;
            </button>
          )}
          {videoUrls
            .slice(thumbnailStart, thumbnailStart + 3)
            .map((url, index) => (
              <div key={thumbnailStart + index} className="thumbnail-item">
                <img
                  src={`https://img.youtube.com/vi/${
                    url.match(/embed\/(.*)\?/)[1]
                  }/0.jpg`}
                  alt={`Thumbnail ${thumbnailStart + index + 1}`}
                  className={
                    currentVideo === thumbnailStart + index
                      ? "active-thumbnail"
                      : ""
                  }
                  onClick={() => handleVideoChange(thumbnailStart + index)}
                />
              </div>
            ))}
          {thumbnailStart < videoUrls.length - 3 && (
            <button
              className="thumbnail-arrow right-arrow"
              onClick={() => handleThumbnailScroll("right")}
            >
              &gt;
            </button>
          )}
        </div>
      </div>
      <style>
        {`.thumbnail-container img {
    width: 100px;
    height: 75px;
    margin: 0 10px;
    cursor: pointer;
  }
  .thumbnail-container img.active-thumbnail {
    border: 2px solid #4285F4;
  }
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif,
      "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, "Courier New", monospace;
  }`}
      </style>
    </div>
  );
};
export default IndexPage;
