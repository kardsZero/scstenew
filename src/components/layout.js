import React from "react"
import "../styles/global.css"
import Navbar from "./Navbar"
import Footer from "./Footer"
import Top from "./Top"
import Header from "./Header"

const Layout = ({ children }) => {
  return (
    <div>
      <Top />
      <Header />
      <Navbar />
      {children}
      <Footer/>
    </div>
  )
}

export default Layout
