import React from "react";

function Map() {
  return (
    <div className="mb-6 ">
        {/* <h2 className="text-4xl p-3 mr-3">Location on Google Map</h2> */}
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3599.10614403571!2d91.89703657384429!3d25.56813516657938!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x37507f9612507eeb%3A0x7ecaf5f19af35712!2sUpper%20Nongrim%20Hills!5e0!3m2!1sen!2sin!4v1697526390066!5m2!1sen!2sin"
        width="1000"
        height="450"
        style={{ border: 0 }}
        allowFullScreen=""
        loading="lazy"
        referrerPolicy="no-referrer-when-downgrade"
      ></iframe>
      
    </div>
  );
}

export default Map;
