import React from "react"
import { graphql, useStaticQuery } from "gatsby"
// import SocialMedia from "../components/Instagram"
// import FaceBook from "../components/FaceBook"

// import Contact from "./Contact"
import Layout from "../components/Layout"
const AboutUsComponent = () => {
  // Use Gatsby's useStaticQuery to fetch data
  const data = useStaticQuery(graphql`
    query {
      allNodeAboutUs {
        nodes {
          title
          relationships {
            field_aboutusimage {
              relationships {
                field_image {
                  localFile {
                    publicURL
                  }
                }
              }
              field_imagedescription {
                processed
              }
            }
            field_aboutustitledescription {
              field_title
              field_description {
                processed
              }
            }
          }
        }
      }
    }
  `)

  // Extract the relevant data
  const aboutUsData = data.allNodeAboutUs.nodes

  return (
    <Layout>
    <div className="">
      {aboutUsData.map((item, index) => (
        <div key={index} className="mb-8">
          {/* <h2 className="text-2xl font-semibold text-center bg-green-800 mb-4">
            {item.title}
          </h2> */}

          <div className="lg:grid lg:grid-cols-1 md:grid-cols-2 gap-4 sm:grid-cols-1">
            {item.relationships.field_aboutusimage.map(
              (imageData, imageIndex) => (
                <div key={imageIndex} className="mb-4">
                  <img
                    src={
                      imageData.relationships.field_image[0].localFile.publicURL
                    }
                    alt={`Image ${imageIndex + 1}`}
                    className="w-full mb-4"
                  />
                </div>
              ),
            )}
              <div className="mx-24">
            <div className="mb-4 mx-6 leading-7 text-lg">
              <h3 className="text-2xl font-semibold mb-2">
                {
                  item.relationships.field_aboutustitledescription[0]
                    .field_title
                }
              </h3>
              <div className="text-gray-700">
                {item.relationships.field_aboutustitledescription[0].field_description.map(
                  (content, contentIndex) => (
                    <div key={contentIndex} className="text-gray-700 mb-2 ml-4">
                      <div
                        dangerouslySetInnerHTML={{
                          __html: content.processed,
                        }}
                      />
                    </div>
                  ),
                )}
              </div>
            </div>

            <div className="lg:flex lg:gap-10 mx-6 my-6">
              <div className="mb-4 flex-1 shadow-lg rounded-lg border p-3">
                <h3 className="text-xl font-semibold mb-2 text-center">
                  {
                    item.relationships.field_aboutustitledescription[2]
                      .field_title
                  }
                </h3>
                <div className="text-gray-700 font-semibold">
                  {item.relationships.field_aboutustitledescription[2].field_description.map(
                    (content, contentIndex) => (
                      <div
                        key={contentIndex}
                        className="text-gray-700 mb-2 leading-7"
                      >
                        <div
                          dangerouslySetInnerHTML={{
                            __html: content.processed,
                          }}
                        />
                      </div>
                    ),
                  )}
                </div>
              </div>

              <div className="mb-4 flex-1 shadow-lg rounded-lg border p-3">
                <h3 className="text-xl font-semibold mb-2 text-center">
                  {
                    item.relationships.field_aboutustitledescription[1]
                      .field_title
                  }
                </h3>
                <div className="text-gray-700 font-semibold">
                  {item.relationships.field_aboutustitledescription[1].field_description.map(
                    (content, contentIndex) => (
                      <div
                        key={contentIndex}
                        className="text-gray-700 mb-2 leading-7"
                      >
                        <div
                          dangerouslySetInnerHTML={{
                            __html: content.processed,
                          }}
                        />
                      </div>
                    ),
                  )}
                </div>
              </div>
            </div>

            <div className="mb-4 mx-6 leading-7 text-lg">
              <h3 className="text-2xl font-semibold mb-2">
                {
                  item.relationships.field_aboutustitledescription[3]
                    .field_title
                }
              </h3>
              <div className="text-gray-700">
                {item.relationships.field_aboutustitledescription[3].field_description.map(
                  (content, contentIndex) => (
                    <ul key={contentIndex} className="text-gray-700 mb-2 ml-4">
                      <li>
                        <span
                          dangerouslySetInnerHTML={{
                            __html: `➡️ ${content.processed.replace(
                              /<\/?p>/g,
                              "",
                            )}`,
                          }}
                        />
                      </li>
                    </ul>
                  ),
                )}
              </div>
            </div>
          </div>
        </div>
        </div>
      ))}
      {/* <SocialMedia/> */}
      {/* <FaceBook/> */}
      {/* <hr className="w-1/2 m-auto"/>
      <h2 className="text-3xl text-center font-bold mb-4 mt-10">Contact Us</h2>
      <Contact/> */}
    </div>
    </Layout>
  )
}

export default AboutUsComponent
