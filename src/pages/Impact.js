import React, { useState } from "react";
import { TECollapse } from "tw-elements-react";
import { graphql } from "gatsby";
import Layout from "../components/Layout";

const Impact = ({ data }) => {
  const [activeElement, setActiveElement] = useState("element0");

  const handleClick = (value) => {
    if (value === activeElement) {
      setActiveElement("");
    } else {
      setActiveElement(value);
    }
  };

  // Check if data and data.allNodeImpacts are defined
  if (!data || !data.allNodeImpacts) {
    return <div>No impact data found.</div>;
  }

  const nodeImpacts = data.allNodeImpacts.nodes;

  return (
    <Layout>
    <div className="lg:mx-64 md:mx-12 sm:mx-8 mt-10">
      <ul>
        {nodeImpacts.map((impact, index) => (
          <li key={index} className="">
            <div className="">
              <h2 className="mb-0" id={`heading${index}`}>
                <button
                  className={`${
                    activeElement === `element${index}` &&
                    `text-primary [box-shadow:inset_0_-1px_0_rgba(229,231,235)] dark:!text-primary-400 dark:[box-shadow:inset_0_-1px_0_rgba(75,85,99)]`
                  } group relative flex w-full items-center rounded-t-[15px] border-0 bg-white px-5 py-4 text-left text-base sm:text-sm md:text-lg text-neutral-800 transition [overflow-anchor:none] hover:z-[2] focus:z-[3] focus:outline-none dark:bg-neutral-800 dark:text-white`}
                  type="button"
                  onClick={() => handleClick(`element${index}`)}
                  aria-expanded={activeElement === `element${index}`}
                  aria-controls={`collapse${index}`}
                >
                  <h2 className="text-xl md:text-2xl font-bold sm:text-sm">{impact.title}</h2>
                  <span
                    className={`${
                      activeElement === `element${index}`
                        ? `rotate-[-180deg] -mr-1`
                        : `rotate-0 fill-[#212529] dark:fill-white`
                    } ml-auto h-5 w-5 shrink-0 fill-[#336dec] transition-transform duration-200 ease-in-out motion-reduce:transition-none dark:fill-blue-300`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth="1.5"
                      stroke="currentColor"
                      className="h-6 w-6"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                      />
                    </svg>
                  </span>
                </button>
              </h2>
              <TECollapse show={activeElement === `element${index}`} className="!mt-0 !rounded-b-none !shadow-none">
                <div className="pt-2">
                  {impact.relationships.field_impactsection.map((section, sectionIndex) => (
                    <div key={sectionIndex} className="p-4 bg-gray-100 shadow-md rounded-lg">
                      <div className="h-32 flex items-center justify-center">
                        <h3 className="font-semibold text-xl md:text-2xl mb-2">{section.field_title}</h3>
                      </div>
                      <ul className="list-disc px-5 leading-7 ">
                        {section.field_description.map((description, idx) => (
                          <li key={idx} className="pl-3 text-lg">
                            <div dangerouslySetInnerHTML={{ __html: description.processed }} />
                          </li>
                        ))}
                      </ul>
                    </div>
                  ))}
                </div>
              </TECollapse>
            </div>
          </li>
        ))}
      </ul>
    </div>
    </Layout>
  );
  
};

export const query = graphql`
  query  {
    allNodeImpacts {
      nodes {
        title
        relationships {
          field_impactsection {
            field_title
            field_description {
              processed
            }
          }
        }
      }
    }
  }
`;

export default Impact;
