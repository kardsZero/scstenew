import React, { useState } from "react";
import { TECollapse } from "tw-elements-react";
import { useStaticQuery, graphql } from "gatsby";
import Layout from "../components/Layout";

const Programmes = () => {
  const data = useStaticQuery(graphql`
    query  {
      allNodeProgrammes {
        nodes {
          title
          field_programmebody {
            processed
          }
        }
      }
    }
  `);

  const programmes = data.allNodeProgrammes ? data.allNodeProgrammes.nodes : [];
  const [activeElement, setActiveElement] = useState(0); // Initialize with the first element open

  const handleClick = (index) => {
    if (index === activeElement) {
      setActiveElement(""); // Close the currently open element when clicking on it again
    } else {
      setActiveElement(index); // Open the clicked element
    }
  };

  return (
    <Layout>
      <div className="lg:mx-96 md:mx-12 sm:mx-8 mt-10">
        <ul>
          {programmes.map((programme, index) => (
            <li key={index} className="">
              <div className="">
                <h2 className="mb-0" id={`heading${index}`}>
                  <button
                    className={`${
                      activeElement === index &&
                      `text-primary [box-shadow:inset_0_-1px_0_rgba(229,231,235)] dark:!text-primary-400 dark:[box-shadow:inset_0_-1px_0_rgba(75,85,99)]`
                    } group relative flex w-full items-center rounded-t-[15px] border-0 bg-white px-5 py-4 text-left text-base sm:text-sm md:text-lg text-neutral-800 transition [overflow-anchor:none] hover:z-[2] focus:z-[3] focus:outline-none dark:bg-neutral-800 dark:text-white`}
                    type="button"
                    onClick={() => handleClick(index)}
                    aria-expanded={activeElement === index}
                    aria-controls={`collapse${index}`}
                  >
                    <h2 className="text-xl md:text-2xl font-bold sm:text-sm">{programme.title}</h2>
                    <span
                      className={`${
                        activeElement === index
                          ? `rotate-[-180deg] -mr-1`
                          : `rotate-0 fill-[#212529] dark:fill-white`
                      } ml-auto h-5 w-5 shrink-0 fill-[#336dec] transition-transform duration-200 ease-in-out motion-reduce:transition-none dark:fill-blue-300`}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="currentColor"
                        className="h-6 w-6"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                        />
                      </svg>
                    </span>
                  </button>
                </h2>
                <TECollapse show={activeElement === index} className="!mt-0 !rounded-b-none !shadow-none">
                  <div className="px-5 py-4">
                    {programme.field_programmebody.map((body, bodyIndex) => (
                      <div key={bodyIndex} className="text-lg">
                        <div dangerouslySetInnerHTML={{ __html: body.processed }} />
                      </div>
                    ))}
                  </div>
                </TECollapse>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </Layout>
  );
};

export default Programmes;
