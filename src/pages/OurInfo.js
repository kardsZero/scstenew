import React from "react"
import AboutUs from "./About"
import ContactUs from "./ContactUs"
// import Map from "../components/Map"
function OurInfo() {
  return (
    <div>
      <div>
        <AboutUs />
      </div>
      <hr className="bg-red-950 w-1/2 h-px  mx-auto" />

      <ContactUs />
      {/* <Map/> */}
    </div>
  )
}

export default OurInfo
