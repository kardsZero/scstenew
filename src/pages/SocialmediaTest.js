import React from 'react'
import Instagram from "../components/Instagram"
import Layout from '../components/Layout'
function SocialmediaTest() {
  return (
    <Layout>
    <div className='mt-10'>
      <Instagram/>
    </div>
    </Layout>
  )
}

export default SocialmediaTest
