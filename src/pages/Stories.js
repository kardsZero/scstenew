import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import StoriesPSP from "./StoriesPSP"
import SuccessStories from "./SuccessStories"
import Layout from "../components/Layout";
const MyComponent = () => {
  // Use Gatsby's useStaticQuery to fetch data
  const data = useStaticQuery(graphql`
    query {
      allNodeStories(filter: { id: { eq: "797e73a9-c45e-580d-8aa9-48c72283e9e2" } }) {
        nodes {
          title
          id
          relationships {
            field_stories_content {
              field_title
              field_description {
                processed
              }
            }
          }
        }
      }
    }
  `);

  // Extract the relevant data
  const storyData = data.allNodeStories.nodes[0]; // Assuming you expect one result

  if (!storyData) {
    return <div>No data found.</div>;
  }

  // Access the data as needed
  const title = storyData.title;
  // const id = storyData.id;
  const content = storyData.relationships.field_stories_content;

  return (
    <Layout>
    <div className="bg-gray-100 p-4 mt-10 mx-28">
      <h1 className="text-3xl font-bold mb-4 text-center">{title}</h1>
      
      <div>
        {content.map((item, index) => (
          <div key={index} className="my-4">
            <h2 className="text-2xl font-semibold mb-2">{item.field_title}</h2>
            <div>
              {item.field_description.map((description, descriptionIndex) => (
                <div key={descriptionIndex} className="mb-2">
                  <div dangerouslySetInnerHTML={{ __html: description.processed }} 
                   className="text-gray-700 text-justify"
                   />
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
      <div>
      <StoriesPSP/>
      </div>
      <div>
        <SuccessStories/>
      </div>
    </div>
    </Layout>
  );
};

export default MyComponent;
