import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import Map from "../components/Map"


const ContactUsComponent = () => {
  // Use Gatsby's useStaticQuery to fetch data
  const data = useStaticQuery(graphql`
    query {
      allNodeContactUs {
        nodes {
          title
          relationships {
            field_contactimage {
              relationships {
                field_image {
                  localFile {
                    publicURL
                  }
                }
              }
              field_imagedescription {
                processed
              }
            }
            field_contacttitledescription {
              field_title
              field_description {
                processed
              }
            }
          }
        }
      }
    }
  `)

  // Extract the relevant data
  const contactUsData = data.allNodeContactUs.nodes

  return (
    <div>
      <div className="p-4 my-20">
        {contactUsData.map((item, index) => (
          <div key={index} className="mb-8">
            <h2 className="text-4xl font-semibold mb-4 text-center">
              {item.title}
            </h2>
            
            <div className="lg:flex items-center justify-between my-6 mx-4 sm:grid sm:grid-cols-1 sm:gap-4 md:grid-cols-2 md:gap-8 lg:mx-28">
              <div>
                {item.relationships.field_contactimage.map(
                  (imageData, imageIndex) => (
                    <div key={imageIndex} className="mb-4">
                      <img
                        src={imageData.relationships.field_image[0].localFile.publicURL}
                        alt={`Image ${imageIndex + 1}`}
                        className="w-full max-w-xs lg:ml-20 h-auto mb-4"
                      />
                      <div className="text-gray-700 text-base mb-4 text-center">
                        <div
                          dangerouslySetInnerHTML={{
                            __html: imageData.field_imagedescription[0].processed,
                          }}
                        />
                      </div>
                    </div>
                  ),
                )}
              </div>
              {/* <div className="mb-20">
                {item.relationships.field_contacttitledescription.map(
                  (description, descriptionIndex) => (
                    <div key={descriptionIndex} className="mb-10">
                      <h3 className="text-2xl sm:text-3xl font-semibold mb-2">
                        {description.field_title}
                      </h3>
                      <div className="text-gray-700 text-base sm:text-lg">
                        {description.field_description.map(
                          (content, contentIndex) => (
                            <div key={contentIndex} className="text-sm sm:text-base mb-2">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: content.processed,
                                }}
                              />
                            </div>
                          ),
                        )}
                      </div>
                    </div>
                  ),
                )}
              </div> */}
            </div>
          </div>
        ))}
      </div>
      <div className="flex justify-center items-center">
        <Map />
      </div>
    </div>
  );
  
  
  
}

export default ContactUsComponent;
