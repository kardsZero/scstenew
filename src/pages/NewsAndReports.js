import React from 'react';
import { graphql } from 'gatsby';

const HomeContent = ({ data }) => {
  const homeNodes = data.allNodeHome.nodes;

  return (
    <div>
      {homeNodes.map((node, nodeIndex) => (
        <div key={nodeIndex}>
          <h2>{node.title}</h2>
          {node.relationships.field_home_content.map((content, contentIndex) => (
            <div key={contentIndex}>
              <h3>{content.field_title}</h3>
              {content.field_description.map((desc, descIndex) => (
                <div key={descIndex}>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: desc.processed,
                    }}
                  />
                </div>
              ))}
            </div>
          ))}
        </div>
      ))}
    </div>
  );
};

export const query = graphql`
  query {
    allNodeHome {
      nodes {
        title
        relationships {
          field_home_content {
            field_title
            field_description {
              processed
            }
          }
        }
      }
    }
  }
`;

export default HomeContent;
