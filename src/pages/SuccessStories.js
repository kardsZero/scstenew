import React from "react";
import { graphql, useStaticQuery } from "gatsby";

const MyComponent = () => {
  // Use Gatsby's useStaticQuery to fetch data
  const data = useStaticQuery(graphql`
    query {
      allNodeStoriesAndImage {
        nodes {
          title
          relationships {
            field_storiesimagedescription {
              relationships {
                field_image {
                  localFile {
                    publicURL
                  }
                }
              }
              field_imagedescription {
                processed
              }
            }
          }
        }
      }
    }
  `);

  // Extract the relevant data
  const storyData = data.allNodeStoriesAndImage.nodes;

  return (
    <div className="bg-gray-100 p-4">
      {storyData.map((item, index) => (
        <div key={index} className="mb-8">
          <h2 className="text-xl font-semibold mb-4">{item.title}</h2>
          {item.relationships.field_storiesimagedescription.map((description, descriptionIndex) => (
            <div key={descriptionIndex} className="mb-4">
              <div className="lg:grid lg:grid-cols-3 gap-2 md:grid md:grid-cols-2 sm:grid-cols-1fr lg:mx-10 ">
                {description.relationships.field_image.map((image, imageIndex) => (
                  <div key={imageIndex} className="mb-4 lg:mb-0">
                    <img
                      src={image.localFile.publicURL}
                      alt={`Image ${imageIndex + 1}`}
                      className="h-56 w-full sm:w-[450px] object-fill shadow-lg"
                    />
                  </div>
                ))}
              </div>
  
              <div className="p-8">
                {description.field_imagedescription.map((content, contentIndex) => (
                  <div key={contentIndex} className="text-gray-700 mb-2 text-justify leading-7 text-base">
                    <div dangerouslySetInnerHTML={{ __html: content.processed }} />
                  </div>
                ))}
              </div>
            </div>
          ))}
        </div>
      ))}
    </div>
  );
  
};

export default MyComponent;
