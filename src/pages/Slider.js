import React from 'react';
import { graphql } from 'gatsby';
import { Carousel } from "react-carousel-minimal"

const Slider = ({ data }) => {
  const sliderImages = data.allParagraphSliderImage.nodes;

  // Generate the imagesForSlider array
  const imagesForSlider = sliderImages.reduce((acc, sliderItem) => {
    return acc.concat(
      sliderItem.relationships.field_home_slider_images.map((image) => ({
        image: image.localFile.publicURL,
        // caption: 'Your Caption Here', // You can add captions based on your data
      }))
    );
  }, []);

  return (
    <div> 
        {console.log("sliderImages::",sliderImages)}
      <Carousel
        data={imagesForSlider}
        time={3000}
        width="100%"
        height="500px"
        slideNumber={false}
        slideNumberStyle={{
          fontSize: "20px",
          fontWeight: "bold",
        }}
        captionPosition="bottom"
        automatic={true}
        dots={true}
        pauseIconColor="white"
        pauseIconSize="40px"
        slideBackgroundColor="darkgrey"
        slideImageFit="cover"
      />
    </div>
  );
};

export const query = graphql`
  query {
    allParagraphSliderImage {
      nodes {
        relationships {
          field_home_slider_images {
            localFile {
              publicURL
            }
          }
        }
      }
    }
  }
`;

export default Slider;
