import React from 'react';
import { Link, graphql } from 'gatsby';
import Layout from '../components/Layout'
import { Carousel } from 'react-carousel-minimal';
import { useState, useEffect } from 'react';
import ScrollToTop from "react-scroll-to-top";
import './index.css'
import { StaticImage } from 'gatsby-plugin-image';
const MyComponent = ({data}) => {

const node1 = data.allParagraphSliderImage.nodes;
const node7 = data.allNodeArticle.nodes [0];
const node6 = data.allNodeHome.nodes [5];
const node2 = data.allNodeHome.nodes [1];
const node3 = data.allNodeHome.nodes [2];
const node4 = data.allNodeHome.nodes [3];
const node5 = data.allNodeHome.nodes [4];

const [activeButton, setActiveButton] = useState(null);
const [activeButtonTwo, setActiveButtonTwo] = useState(null);
const [paragraphone, setParagraphOne] = useState(true);
const [paragraphtwo, setParagraphTwo] = useState(false);
const [paragraphthree, setParagraphThree] = useState(false);
const [paragraphfour, setParagraphFour] = useState(false);
const [paragraphfive, setParagraphFive] = useState(true);
const [paragraphsix, setParagraphSix] = useState(false);
const [paragraphseven, setParagraphSeven] = useState(false);
const [paragrapheight, setParagraphEight] = useState(false);

const handleButtonOneClick = () => {
  setParagraphOne(true);
  setParagraphTwo(false);
  setParagraphThree(false);
  setParagraphFour(false);
  setActiveButton(0);
}
const handleButtonTwoClick = () => {
  setParagraphTwo(true);
  setParagraphOne(false);
  setParagraphThree(false);
  setParagraphFour(false);
  setActiveButton(1);
}
const handleButtonThreeClick = () => {
  setParagraphThree(true);
  setParagraphTwo(false);
  setParagraphOne(false);
  setParagraphFour(false);
  setActiveButton(2);
}
const handleButtonFourClick = () => {
  setParagraphFour(true);
  setParagraphTwo(false);
  setParagraphThree(false);
  setParagraphOne(false);
  setActiveButton(3);
}
const buttonFive = () => {
  setParagraphFive(true);
  setParagraphSix(false);
  setParagraphSeven(false);
  setParagraphEight(false);
  setActiveButtonTwo(0);
}
const buttonSix = () => {
  setParagraphSix(true);
  setParagraphFive(false);
  setParagraphSeven(false);
  setParagraphEight(false);
  setActiveButtonTwo(1);
}
const buttonSeven = () => {
  setParagraphSeven(true);
  setParagraphFive(false);
  setParagraphSix(false);
  setParagraphEight(false);
  setActiveButtonTwo(2);
}
const buttonEight = () => {
  setParagraphEight(true);
  setParagraphFive(false);
  setParagraphSix(false);
  setParagraphSeven(false);
  setActiveButtonTwo(3);
}

useEffect(() => {
  // Set the first button as active when the component mounts
  setActiveButton(0);
  setActiveButtonTwo(0);
}, []); 

  const imagesForSlider = node1.flatMap(node =>
    node.relationships.field_home_slider_images.map(image => ({
      image: image.localFile.publicURL,
    }))
  );

  return (
    <Layout>
      <div>
      <Carousel
          data={imagesForSlider}
          time={3000}
          width="100%"
          height="500px"
          slideNumber={true}
          slideNumberStyle={{
            fontSize: '20px',
            fontWeight: 'bold',
          }}
          captionPosition="bottom"
          automatic={true}
          dots={true}
          pauseIconColor="white"
          pauseIconSize="40px"
          slideBackgroundColor="darkgrey"
          slideImageFit="cover"
        />
      </div>
      <div id="main-content" className='grid grid-cols-2 gap-x-10 mx-10 mt-10'>
        <div className='shadow-md w-full h-fit'>
              <div className='px-5 py-2'>
                <h1 className='text-2xl font-bold bg-blue-900 py-2 px-2 text-white'>{node2.title}</h1>
                  <marquee scrollamount="3" direction="up">
                    <div className="-mt-5">
                    <h1 className='font-bold'>{node2.relationships.field_home_content[0].field_title}</h1>
                      <div className='-mt-10'>
                        {
                          node2.relationships.field_home_content[0].field_description.map((item, key) => (
                           <div key={key} className='leading-3 -mt-3'>
                           <p dangerouslySetInnerHTML={{ __html: item.processed }}></p>
                           </div>
                          ))
                        }
                      </div>
                    </div>
                    <div>
                      <h1 className='font-bold'>{node2.relationships.field_home_content[1].field_title}</h1>
                      <div className="-mt-10">
                      {
                        node2.relationships.field_home_content[1].field_description.map((item, key) => (
                         <div key={key} className='leading-3 -mt-3'>
                         <p dangerouslySetInnerHTML={{ __html: item.processed }}></p>
                         </div>
                        ))
                      }
                      </div>
                    </div>
                    <div>
                      <h1 className='font-bold'>{node2.relationships.field_home_content[2].field_title}</h1>
                      <div className="-mt-10">
                      {
                        node2.relationships.field_home_content[2].field_description.map((item, key) => (
                         <div key={key} className='leading-3 -mt-3 border-t-2 pt-2'>
                         <p dangerouslySetInnerHTML={{ __html: item.processed }}></p>
                         </div>
                        ))
                      }
                      </div>
                    </div>
                  </marquee>
              </div>
        </div>
        <div className='flex flex-col gap-y-10'>
          <div className='grid grid-cols-2 px-5 py-2 -space-x-36 shadow-md'>
              <div>
                <StaticImage className='w-1/2' src="../images/Gunanka-DB-IFS.jpeg" />
              </div>
              <div className='place-self-center'>
                <h1 className='font-bold text-lg'>Shri Gunanka D.B.,IFS</h1>
                <h2 className='text-lg'>Member Secretary</h2>
                <h2 className='text-lg'>State Council of Science, Technology & Environment (SCSTE), MEGHALAYA</h2>
              </div>
          </div>
          <div className='shadow-md w-full h-4/5'>
                <div className='px-5 py-2'>
                  <h1 className='text-2xl font-bold bg-blue-900 py-2 px-2 text-white'>{node3.title}</h1>
                  <div className="-mt-5">
                  <h1 className='font-bold'>{node3.relationships.field_home_content[0].field_title}</h1>
                    <div className='-mt-10'>
                      {
                        node3.relationships.field_home_content[0].field_description.map((item, key) => (
                         <div key={key} className='leading-3 -mt-3 border-t-2 pt-2'>
                         <p dangerouslySetInnerHTML={{ __html: item.processed }}></p>
                         </div>
                        ))
                      }
                    </div>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <div className='mx-10 mt-10'>
        <div className='px-5 py-2 shadow-md'>
          <h1 className='text-center text-2xl font-bold bg-blue-900 py-2 text-white'>{node4.relationships.field_home_content[0].field_title}</h1>
            <div className='-mt-5'>
            <div 
            className="text-justify text-lg" 
            dangerouslySetInnerHTML={{ __html: node4.relationships.field_home_content[0].field_description[0].processed }}>
            </div>
            <div 
            className="text-justify text-lg text-red-500" 
            dangerouslySetInnerHTML={{ __html: node4.relationships.field_home_content[0].field_description[1].processed }}>
            </div>
            </div>
            <div className='grid grid-cols-2 gap-x-2'>
              <div className="-space-y-5">
                <img className='w-1/4 mx-auto' src={node4.relationships.field_upload_image[0].localFile.publicURL} />
                <h1 className="text-center text-xl" dangerouslySetInnerHTML={{ __html: node4.relationships.field_home_content[0].field_description[2].processed }}></h1>
                <table className="mx-auto text-center bg-gray-500 w-4/5 h-1/5 text-white">
                  <tr>
                    <th className="border-2">District/Block Level</th>
                    <th className="border-2">State Level</th>
                    <th className="border-2">National Level</th>
                  </tr>
                  <tr>
                    <td className="border-2">Sept-Oct 2023</td>
                    <td className="border-2">Nov 2023</td>
                    <td className="border-2">Dec 2023</td>
                  </tr>
                </table>
                  <div className='gap-x-2 gap-y-2 pt-7 grid grid-cols-3'>
                    <Link to={node4.field_urls[0].uri} 
                    className='no-underline text-white bg-blue-500 py-2 px-2 rounded-lg shadow-xl hover:bg-blue-700 text-center'>
                      {node4.field_urls[0].title}
                      </Link>
                    <Link to={node4.field_urls[1].uri} 
                    className='no-underline text-white bg-blue-500 py-2 px-2 rounded-lg shadow-xl hover:bg-blue-700 text-center'>
                      {node4.field_urls[1].title}
                      </Link>
                    <Link to={node4.field_urls[2].uri} 
                    className='no-underline text-white bg-blue-500 py-2 px-2 rounded-lg shadow-xl hover:bg-blue-700 text-center'>
                      {node4.field_urls[2].title}
                      </Link>
                    <Link to={node4.field_urls[3].uri} 
                    className='no-underline text-white bg-blue-500 py-2 px-2 rounded-lg shadow-xl hover:bg-blue-700 text-center'>
                      {node4.field_urls[3].title}
                      </Link>
                    <Link to={node4.field_urls[4].uri} 
                    className='no-underline text-white bg-blue-500 py-2 px-2 rounded-lg shadow-xl hover:bg-blue-700 text-center'>
                      {node4.field_urls[4].title}
                      </Link>
                    <Link to={node4.field_urls[5].uri} 
                    className='no-underline text-white bg-blue-500 py-2 px-2 rounded-lg shadow-xl hover:bg-blue-700 text-center'>
                      {node4.field_urls[5].title}
                      </Link>
                </div>
              </div>
              <div>
                <img src={node4.relationships.field_upload_image[1].localFile.publicURL} />
              </div>
            </div>
        </div>
      </div>
      <div className='mx-10 mt-10'>
        <div className='px-5 py-2 shadow-md'>
          <h1 className='text-center text-2xl font-bold bg-blue-900 py-2 text-white'>{node5.title}</h1>
          <center>
          <div className='space-x-10 -mt-5'>
      <button
        className={`bg-gray-500 py-2 px-2 text-white hover:border-b-4 border-black ${
          activeButton === 0 ? 'active' : ''
        }`}
        onClick={handleButtonOneClick}
      >
        {node5.relationships.field_home_content[0].field_title}
      </button>
      <button
        className={`bg-gray-500 py-2 px-2 text-white hover:border-b-4 border-black ${
          activeButton === 1 ? 'active' : ''
        }`}
        onClick={handleButtonTwoClick}
      >
        {node5.relationships.field_home_content[1].field_title}
      </button>
      <button
        className={`bg-gray-500 py-2 px-2 text-white hover:border-b-4 border-black ${
          activeButton === 2 ? 'active' : ''
        }`}
        onClick={handleButtonThreeClick}
      >
        {node5.relationships.field_home_content[2].field_title}
      </button>
      <button
        className={`bg-gray-500 py-2 px-2 text-white hover:border-b-4 border-black ${
          activeButton === 3 ? 'active' : ''
        }`}
        onClick={handleButtonFourClick}
      >
        {node5.relationships.field_home_content[3].field_title}
      </button>
    </div>
          </center>
          <div className='pt-5'>
            {paragraphone && (
              <div className='flex gap-x-5 px-60'>
                <div className="text-justify text-lg" 
                dangerouslySetInnerHTML={{ __html: node5.relationships.field_home_content[0].field_description[0].processed }}></div>
                <img src={node5.relationships.field_upload_image[0].localFile.publicURL} />
              </div>
            )}
            {paragraphtwo && (
              <div className='flex gap-x-5 px-60'>
                <div className="text-justify text-lg" 
                dangerouslySetInnerHTML={{ __html: node5.relationships.field_home_content[1].field_description[0].processed }}></div>
                <img src={node5.relationships.field_upload_image[1].localFile.publicURL} />
              </div>
            )}
            {paragraphthree && (
              <div className='flex gap-x-5 px-60'>
                <div className="text-justify text-lg"
                 dangerouslySetInnerHTML={{ __html: node5.relationships.field_home_content[2].field_description[0].processed }}></div>
                <img src={node5.relationships.field_upload_image[2].localFile.publicURL} />
              </div>
            )}
            {paragraphfour && (
              <div className='px-60'>
                <div className="text-justify text-lg"
                 dangerouslySetInnerHTML={{ __html: node5.relationships.field_home_content[3].field_description[0].processed }}></div>
                <div className="text-justify text-lg"
                 dangerouslySetInnerHTML={{ __html: node5.relationships.field_home_content[3].field_description[1].processed }}></div>
                <div className="text-justify text-lg"
                 dangerouslySetInnerHTML={{ __html: node5.relationships.field_home_content[3].field_description[2].processed }}></div>
                <div className="text-justify text-lg"
                 dangerouslySetInnerHTML={{ __html: node5.relationships.field_home_content[3].field_description[3].processed }}></div>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className='mx-10 mt-10'>
        <div className='px-5 py-2 shadow-md'>
          <h1 className='text-center text-2xl font-bold bg-blue-900 py-2 text-white'>{node6.title}</h1>
          <h1 class="text-center -mt-5 text-xl">
                  <span class="font-bold">M: </span>Meghalaya 
                  <span class="font-bold"> Y: </span>Youth – 
                  <span class="font-bold"> S: </span>Specific 
                  <span class="font-bold"> P: </span>Project 
                  <span class="font-bold"> A: </span>Aimed at 
                  <span class="font-bold"> C: </span>Creating 
                  <span class="font-bold"> E: </span>Entrepreneurs
            </h1>
          <center className='-mt-5 pb-2'>
            <Link className='no-underline text-white bg-blue-500 px-2 py-2 rounded-lg hover:bg-blue-700' 
            to={node6.field_urls[0].uri}>{node6.field_urls[0].title}</Link>
          </center>
            <div className='space-x-10 pt-5 grid grid-cols-4'>
              <button onClick={buttonFive} 
                      className={`bg-gray-500 py-2 px-2 text-white hover:border-b-4 border-black ${
                        activeButtonTwo === 0 ? 'active' : ''
                      }`}>
                {node6.relationships.field_home_content[1].field_title}</button>
              <button onClick={buttonSix} 
                      className={`bg-gray-500 py-2 px-2 text-white hover:border-b-4 border-black ${
                        activeButtonTwo === 1 ? 'active' : ''
                      }`}>
                {node6.relationships.field_home_content[2].field_title}</button>
              <button onClick={buttonSeven} 
                      className={`bg-gray-500 py-2 px-2 text-white hover:border-b-4 border-black ${
                        activeButtonTwo === 2 ? 'active' : ''
                      }`}>
                {node6.relationships.field_home_content[3].field_title}</button>
              <button onClick={buttonEight} 
                      className={`bg-gray-500 py-2 px-2 text-white hover:border-b-4 border-black ${
                        activeButtonTwo === 3 ? 'active' : ''
                      }`}>
                {node6.relationships.field_home_content[4].field_title}</button>
            </div>
            <div className='pt-5'>
              {paragraphfive && (
                <div className='text-justify text-lg' 
                dangerouslySetInnerHTML={{ __html: node6.relationships.field_home_content[1].field_description[0].processed }}>
                </div>
              )}
              {paragraphsix && (
                <div className='text-justify text-lg' 
                dangerouslySetInnerHTML={{ __html: node6.relationships.field_home_content[2].field_description[0].processed }}>
                </div>
              )}
              {paragraphseven && (
                <div className='text-justify text-lg' 
                dangerouslySetInnerHTML={{ __html: node6.relationships.field_home_content[3].field_description[0].processed }}>
                </div>
              )}
              {paragrapheight && (
                <div className='text-justify text-lg' 
                dangerouslySetInnerHTML={{ __html: node6.relationships.field_home_content[4].field_description[0].processed }}>
                </div>
              )}
            </div>
        </div>
      </div>
      <div className='mx-10 mt-10 mb-5'>
        <div className='px-5 py-2 shadow-md'>
        <h1 className='text-center text-2xl font-bold bg-blue-900 py-2 text-white'>{node7.title}</h1>
          <div className='grid grid-cols-3 gap-x-2 -mt-5 pb-5'>
            <div className="border px-2 py-2">
              <img className="w-full" src={node7.relationships.field_image[1].localFile.publicURL} />
              <h1 className="text-xl font-bold -mt-5">Programmes</h1>
              <hr className="-mt-10" />
              <ul className="list-disc -mt-5">
              <li>Popularisation of Science Programme</li>
              <li>S&T Entrepreneurship Development Programme</li>
              <li>Introduction of Appropriate Technology</li>
              <li>S&T Library & Documentation Programme</li>
              <li>Specific Projects Programme</li>
              <li>Science Centres Programme</li>
              <li>Students’ Projects Programme</li>
              <li>Remote Sensing Application Programme</li>
              </ul>
              <center className='pt-6'>
                <Link className='no-underline text-center text-white bg-blue-500 py-2 px-2 rounded-lg hover:bg-blue-700'
                to="https://docs.google.com/forms/d/1q-NFbDhLGj-YXGoMPBQ5aAoSn3jO4HPxElhRIIdL3QU/viewform?edit_requested=true">
                  View All Programmes
                  </Link>
              </center>
            </div>
            <div className="border px-2 py-2">
              <img className="w-full" src={node7.relationships.field_image[0].localFile.publicURL} />
              <h1 className="text-xl font-bold -mt-5">Activities & Success Stories</h1>
              <hr className="-mt-10" />
              <ul className="list-disc -mt-5">
              <li>Technology Division</li>
              <li>Innovation and Incubation Division</li>
              <li>Common Facilitating Centre on GIS for school children/colleges/VEC</li>
              <li>Popularization of Science Programme Division</li>
              <li>Action – research Division</li>
              <li>Knowledge Centre</li>
              </ul>
              <center className='space-x-2 pt-20 -mt-2'>
                <Link className='no-underline text-center text-white bg-blue-500 py-2 px-2 rounded-lg hover:bg-blue-700'
                to="https://docs.google.com/forms/d/1q-NFbDhLGj-YXGoMPBQ5aAoSn3jO4HPxElhRIIdL3QU/viewform?edit_requested=true">
                  View All Activities
                  </Link>
                  <Link className='no-underline text-center text-white bg-blue-500 py-2 px-2 rounded-lg hover:bg-blue-700'
                to="https://docs.google.com/forms/d/1q-NFbDhLGj-YXGoMPBQ5aAoSn3jO4HPxElhRIIdL3QU/viewform?edit_requested=true">
                  View All Success Stories
                  </Link>
              </center>
            </div>
            <div className="border px-2 py-2">
              <img className="w-full" src={node7.relationships.field_image[2].localFile.publicURL} />
              <h1 className="text-xl font-bold -mt-5">Impact</h1>
              <hr className="-mt-10" />
              <ul className="list-disc -mt-5">
              <li>Impact of Scientific R&D of appropriate Technology Programme</li>
              <li>Impact of S&T Oriented Entrepreneurship Development Programme</li>
              <li>Impact on Income – Generation Technologies</li>
              <li>Popularisation of Science Programme</li>
              <li>Programme associated with Vigyan Prasar, National Council for Science and Technology Communication, Department of Science & Technology, GOI etc</li>
              <li>Convergence Programme</li>
              </ul>
              <center className='pb-2'>
                <Link className='no-underline text-center text-white bg-blue-500 py-2 px-2 rounded-lg hover:bg-blue-700'
                to="https://docs.google.com/forms/d/1q-NFbDhLGj-YXGoMPBQ5aAoSn3jO4HPxElhRIIdL3QU/viewform?edit_requested=true">
                  View All Impact
                  </Link>
              </center>
            </div>
          </div>
        </div>
      </div>
      <ScrollToTop smooth className="bg-gray-500 pl-1" />
    </Layout>
  );
};

export const query = graphql
`
query {
  allParagraphSliderImage {
    nodes {
      relationships {
        field_home_slider_images {
          localFile {
          publicURL
        }
      }
    }
  }
}
allNodeHome {
  nodes {
    id
    title
    relationships {
      field_home_content {
        field_title
        field_description {
          processed
        }
      }
      field_upload_image {
          localFile {
          publicURL
        }
      }
    }
    field_urls {
      title
      uri
    }
  }
}
allNodeArticle {
  nodes {
    title
    relationships {
      field_image {
        localFile {
          publicURL
        }
      }
    }
  }
}
}
`

export default MyComponent;