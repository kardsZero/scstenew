import React from "react"
import Map from "../components/Map"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEnvelope } from "@fortawesome/free-solid-svg-icons"
import { faPhone } from "@fortawesome/free-solid-svg-icons"
import { faLocationDot } from "@fortawesome/free-solid-svg-icons"
import Layout from "../components/Layout"
function Contact() {
  return (
    <Layout>
    <div className="flex flex-col lg:flex-row mx-4 lg:mx-24 mt-10">
      {/* Left Column: Contact Information */}
      <div className="lg:w-1/2 p-6 order-2">
        <div className="bg-white shadow-md p-4 rounded-lg">
          <p className="text-lg mb-4">
            <FontAwesomeIcon icon={faLocationDot} />
            <span className="font-bold pl-2">Address:</span> STATE COUNCIL OF
            SCIENCE TECHNOLOGY AND ENVIRONMENT Meghalaya State Housing Financing
            Cooperative Society Ltd., Nongrim Hills, Behind Bethany Hospital,
            Shillong - 793003
          </p>
          <p className="text-lg mb-4">
            <FontAwesomeIcon icon={faPhone} />
            <span className="font-bold pl-2">Phone No.:</span> +91 364 2522077
          </p>
          <p className="text-lg">
            <FontAwesomeIcon icon={faEnvelope} />
            <span className="font-bold pl-2">Email Id:</span>{" "}
            stcouncil-megh@meghalaya.gov.in
          </p>
        </div>
      </div>

      {/* Right Column: Map */}
      {/* <div className="lg:w-1/2 p-6 order-1">
        
      </div> */}
      <Map className="md:block sm:block md:w-1/2 p-6 order-1" />
    </div>
    </Layout>
  )
}

export default Contact
